package com.example.usuario.appcartavirtual;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class BebidasActivity extends Activity {

    String[] nombresP = {"Gaseosas", "Capuccino","Jugo", "Vino", "Cerveza", "Vaso con agua", "Tintico", "Limonada"};
    int[] imagesP = {R.drawable.cocacola, R.drawable.capuccino, R.drawable.jugo, R.drawable.vino, R.drawable.cerveza, R.drawable.agua, R.drawable.cafe, R.drawable.limonada};
    String[] descriptionP = {"$1200","$3000","$2000","$27000","$2300","$23000","$700","$1800"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bebidas);

        ListView lista = (ListView)findViewById(R.id.listView);

        CustomAdapter customAdapter = new CustomAdapter();
        lista.setAdapter(customAdapter);
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return imagesP.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.customlayout,null);
            ImageView imageView = (ImageView)view.findViewById(R.id.imageView);
            TextView textView_name =(TextView)view.findViewById(R.id.textView_name);
            TextView textView_dse = (TextView)view.findViewById(R.id.textView_desc);
            imageView.setImageResource(imagesP[i]);
            textView_name.setText(nombresP[i]);
            textView_dse.setText(descriptionP[i]);
            return view;
        }
    }
}
