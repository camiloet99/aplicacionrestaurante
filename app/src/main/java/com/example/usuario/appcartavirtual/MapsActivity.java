package com.example.usuario.appcartavirtual;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng rNutibara, rLa70, rUpb;
        LatLng principal = new LatLng(6.240768, -75.587867);
        rNutibara = new LatLng(6.242336, -75.592781);
        rLa70 = new LatLng(6.245458, -75.589248);
        rUpb = new LatLng(6.243527, -75.589962);
        mMap.addMarker(new MarkerOptions().position(rNutibara).title("Sede Nutibara"));
        mMap.addMarker(new MarkerOptions().position(rLa70).title("Sede la 70"));
        mMap.addMarker(new MarkerOptions().position(rUpb).title("Sede Universidad Pontificia Bolivariana"));
        mMap.addMarker(new MarkerOptions().position(principal).title("Sede Unicentro").snippet("Restaurante principal"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(principal,15));
    }
}
