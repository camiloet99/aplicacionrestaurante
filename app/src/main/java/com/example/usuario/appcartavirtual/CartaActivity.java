package com.example.usuario.appcartavirtual;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class CartaActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carta);
    }

    void platosFuertes(View view){
        Intent intencion = new Intent(this, PlatoFuerteActivity.class);
        startActivity(intencion);
    }

    void bebidas(View view){
        Intent intencion = new Intent(this, BebidasActivity.class);
        startActivity(intencion);
    }

    void entradas(View view){
        Intent intencion = new Intent(this, EntradasActivity.class);
        startActivity(intencion);
    }

    void postres(View view){
        Intent intencion = new Intent(this, PostresActivity.class);
        startActivity(intencion);
    }
}
