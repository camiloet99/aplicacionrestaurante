package com.example.usuario.appcartavirtual;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {

    private final String whatsApp = "com.whatsapp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    void verMapa(View view){
        Intent intencion = new Intent(this, MapsActivity.class);
        startActivity(intencion);
    }

    void verCarta(View view){
        Intent intencion = new Intent(this, CartaActivity.class);
        startActivity(intencion);
    }

    void reservarMesa(View view){
        PackageManager pm = getPackageManager();
        Intent intent = pm.getLaunchIntentForPackage(whatsApp);
        startActivity(intent);
    }

}
