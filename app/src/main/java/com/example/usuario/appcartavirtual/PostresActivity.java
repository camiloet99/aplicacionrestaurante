package com.example.usuario.appcartavirtual;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class PostresActivity extends Activity {

    String[] nombresP = {"Banana Split", "CheeseCake","Perro de chocolate", "Helado", "Malteadas", "Tiramisú", "Bombones de chocolate"};
    int[] imagesP = {R.drawable.bananasplit, R.drawable.cheescacke, R.drawable.chocolate, R.drawable.helado, R.drawable.malteada, R.drawable.tiramisu, R.drawable.bombones};
    String[] descriptionP = {"$5000","$4300","$400000","$2000","$7300","$8000","$6000", "$6400"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plato_fuerte);

        ListView lista = (ListView)findViewById(R.id.listView);

        CustomAdapter customAdapter = new CustomAdapter();
        lista.setAdapter(customAdapter);
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return imagesP.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.customlayout,null);
            ImageView imageView = (ImageView)view.findViewById(R.id.imageView);
            TextView textView_name =(TextView)view.findViewById(R.id.textView_name);
            TextView textView_dse = (TextView)view.findViewById(R.id.textView_desc);
            imageView.setImageResource(imagesP[i]);
            textView_name.setText(nombresP[i]);
            textView_dse.setText(descriptionP[i]);
            return view;
        }
    }
}
