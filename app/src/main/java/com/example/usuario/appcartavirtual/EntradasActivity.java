package com.example.usuario.appcartavirtual;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class EntradasActivity extends Activity {

    String[] NAMES = {"Sopa fría de tomate", "Ceviche de mango","Aguacates rellenos con ensalada de huevo", "Ensalada de garbanzos", "Empanadas de carne", "Ceviche de camarón", "Brochetas de atún"};
    int[] IMAGES = {R.drawable.sopatomate, R.drawable.cevichemango, R.drawable.aguacates, R.drawable.ensaladagarbanzo, R.drawable.empanadas, R.drawable.cevichecamaron, R.drawable.brochetasatun};
    String[] DESCRIPTIONS = {"$5000","$6400","$3500","$7400","$3800","$6300","$9900"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entradas);

        ListView lista = (ListView)findViewById(R.id.listView);

        CustomAdapter customAdapter = new CustomAdapter();
        lista.setAdapter(customAdapter);
    }

    class CustomAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return IMAGES.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.customlayout,null);
            ImageView imageView = (ImageView)view.findViewById(R.id.imageView);
            TextView textView_name =(TextView)view.findViewById(R.id.textView_name);
            TextView textView_dse = (TextView)view.findViewById(R.id.textView_desc);
            imageView.setImageResource(IMAGES[i]);
            textView_name.setText(NAMES[i]);
            textView_dse.setText(DESCRIPTIONS[i]);
            return view;
        }
    }
}
